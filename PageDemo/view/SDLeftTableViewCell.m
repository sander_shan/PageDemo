//
//  SDLeftTableViewCell.m
//  PageDemo
//
//  Created by shansander on 2018/2/5.
//  Copyright © 2018年 shansander. All rights reserved.
//

#import "SDLeftTableViewCell.h"
#import "SDLeftDataModel.h"
@interface SDLeftTableViewCell()

@property (nonatomic, weak) UIImageView * theLeftImageView;

@property (nonatomic, weak) UILabel * theLeftNameLabel;
@end

@implementation SDLeftTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (SDLeftDataModel * )tagertModel
{
    return (SDLeftDataModel * )self.dataModel;
}
- (void)loadHomeDataModel:(SDHomeBaseDataModel *)model
{
    [super loadHomeDataModel:model];
//    self.theLeftNameLabel.
    
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    
    self.theLeftImageView.image = [UIImage imageNamed:[self tagertModel].imageView];
    
    self.theLeftNameLabel.text = [self tagertModel].contentText;
    
    CGSize size = [self.theLeftNameLabel.text boundingRectWithSize:CGSizeMake(DSize(100), MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.theLeftNameLabel.font} context:nil].size;
    
    [self.theLeftNameLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(size);
    }];
    
    [self.theLeftImageView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView).offset(-DSize(size.height/2.f));
    }];
    
    if ([self tagertModel].isSelected) {
        self.contentView.backgroundColor = [UIColor colorForHex:@"#AD9865"];
    }else{
        self.contentView.backgroundColor = [UIColor clearColor];
    }
    
}

- (UIImageView *)theLeftImageView
{
    if (!_theLeftImageView) {
        UIImageView * theView = [[UIImageView alloc] init];
        [self.contentView addSubview:theView];
        [theView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView);
            make.width.and.height.mas_equalTo(DSize(30));
            make.centerY.equalTo(self.contentView);
        }];
        theView.contentMode = UIViewContentModeScaleAspectFit;
    
        _theLeftImageView = theView;
    }
    return _theLeftImageView;
}

- (UILabel *)theLeftNameLabel
{
    if (!_theLeftNameLabel) {
        UILabel * theView = [[UILabel alloc] init];
        [self.contentView addSubview:theView];
        [theView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView);
            make.top.equalTo(self.theLeftImageView.mas_bottom).offset(DSize(10));
            make.size.mas_equalTo(CGSizeZero);
        }];
        theView.font = [UIFont systemFontOfSize:12];
        theView.numberOfLines = 0;
        theView.textColor = [UIColor whiteColor];
        theView.textAlignment = NSTextAlignmentCenter;
        _theLeftNameLabel = theView;
    }
    return _theLeftNameLabel;
}

@end
