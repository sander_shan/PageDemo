//
//  SDHomeBaseTableViewCell.m
//  NestHouse
//
//  Created by shansander on 2017/3/29.
//  Copyright © 2017年 sander. All rights reserved.
//

#import "SDHomeBaseTableViewCell.h"

@implementation SDHomeBaseTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];

    }
    return self;
}

+ (NSString *)getCellIdentifier
{
    return NSStringFromClass(self);
}


- (void)loadHomeDataModel:(SDHomeBaseDataModel * )model
{
    _dataModel = model;

    [self.contentView setBackgroundColor:[UIColor whiteColor]];

//    self.textLabel.text = NSStringFromClass([self class]);
}
- (void)refeshFrame
{
    
}


- (CGSize)sizeThatFits:(CGSize)size
{
    return CGSizeMake(size.width, self.height_statistics_cell);
}


- (UITableView *)supertableView
{
    UIView *tableView = self.superview;
    while (![tableView isKindOfClass:[UITableView class]] && tableView) {
        tableView = tableView.superview;
    }
    return (UITableView *)tableView;
}
@end
