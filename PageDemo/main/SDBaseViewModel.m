//
//  SDBaseViewModel.m
//  NestHouse
//
//  Created by shansander on 2017/5/12.
//  Copyright © 2017年 黄建国. All rights reserved.
//

#import "SDBaseViewModel.h"

@implementation SDBaseViewModel

+( SDBaseViewModel *)modelWithViewController:(UIViewController *)viewController;
{
    SDBaseViewModel * viewModel = [[self alloc] initWithViewController:viewController];
    return viewModel;
}

- (instancetype)initWithViewController:(UIViewController *)viewController
{
    self = [super init];
    if (self) {
        _viewController = viewController;
        
    }
    return self;
}





@end
