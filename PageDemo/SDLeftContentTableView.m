//
//  SDLeftContentTableView.m
//  PageDemo
//
//  Created by shansander on 2018/2/5.
//  Copyright © 2018年 shansander. All rights reserved.
//

#import "SDLeftContentTableView.h"

#import "SDHomeBaseDataModel.h"

#import "SDLeftTableViewCell.h"

#import "SDLeftDataModel.h"

@interface SDLeftContentTableView()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, weak) UITableView * theLeftContentTableView;

@property (nonatomic, strong) NSArray * tableList;

@property (nonatomic, weak) UIButton * theExpanceButton;

@end

@implementation SDLeftContentTableView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self sd_configView];
        [self sd_configData];
    }
    return self;
}

- (void)sd_configData
{
    SDLeftDataModel * model1 = [[SDLeftDataModel alloc] init];
    model1.imageView = @"icon_sidebar_01";
    model1.contentText = @"Skin Questionaire";
    SDLeftDataModel * model2 = [[SDLeftDataModel alloc] init];
    model2.imageView = @"icon_sidebar_02";
    model2.contentText = @"Magic BingASnaglysis";
    SDLeftDataModel * model3 = [[SDLeftDataModel alloc] init];
    model3.imageView = @"icon_sidebar_03";
    model3.contentText = @"Product Catalog";
    SDLeftDataModel * model4 = [[SDLeftDataModel alloc] init];
    model4.imageView = @"icon_sidebar_04";
    model4.contentText = @"Personal Beauty Guide";
    
    self.tableList = @[model1,model2,model3,model4];
    
}

- (void)sd_configView
{
    [self theLeftContentTableView];
    self.backgroundColor = [UIColor clearColor];
    
    [self theExpanceButton];
}

- (void)on_expance_button:(id)sender
{
    UIButton * bt = (UIButton * )sender;
    
    bt.selected = !bt.isSelected;
    if (bt.isSelected) {
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.superview);
        }];
        self.theLeftContentTableView.hidden = false;
        self.theLeftContentTableView.backgroundColor = [UIColor colorForHex:@"#242424"];
    }else{
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.superview).offset( - DSize(100) + STATUS_BAR_OFFSET);
        }];
        self.theLeftContentTableView.hidden = true;
        self.theLeftContentTableView.backgroundColor = [UIColor clearColor];
    }
    
   
    
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.tableList.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SDHomeBaseDataModel * model = self.tableList[indexPath.row];
    SDHomeBaseTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:[model CellIdentifier]];
    [cell loadHomeDataModel:model];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SDHomeBaseDataModel * model = self.tableList[indexPath.row];
    return model.hegiht_size_cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SDLeftDataModel * index_model = self.tableList[indexPath.row];
    for (SDLeftDataModel *model  in self.tableList) {
        model.isSelected = false;
    }
    index_model.isSelected  =true;
    
    [tableView reloadData];
    
}

- (UITableView *)theLeftContentTableView
{
    if (!_theLeftContentTableView) {
        UITableView * theView = [[UITableView alloc] init];
        [self addSubview:theView];
        [theView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.top.equalTo(self);
            make.right.equalTo(self).offset(DSize(-20));
            make.bottom.equalTo(self);
            
        }];
        theView.dataSource = self;
        theView.delegate = self;
        [theView registerClass:[SDLeftTableViewCell class] forCellReuseIdentifier:[SDLeftTableViewCell getCellIdentifier]];
        theView.tableFooterView = [UIView new];
        theView.separatorStyle = UITableViewCellSeparatorStyleNone;
        theView.backgroundColor = [UIColor colorForHex:@"#242424"];
        _theLeftContentTableView = theView;
    }
    return _theLeftContentTableView;
}

- (UIButton *)theExpanceButton
{
    if (!_theExpanceButton) {
        UIButton * theView = [[UIButton alloc] init];
        [self addSubview:theView];
        [theView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self);
            make.centerY.equalTo(self);
            make.width.mas_equalTo(DSize(20));
            make.height.mas_equalTo(DSize(100));
        }];
        [theView setImage:[UIImage imageNamed:@"sidebar_mini_01"] forState:UIControlStateNormal];
        [theView setImage:[UIImage imageNamed:@"sidebar_mini_02"] forState:UIControlStateSelected];
        [theView addTarget:self action:@selector(on_expance_button:) forControlEvents:UIControlEventTouchUpInside];
        
        _theExpanceButton = theView;
    }
    return _theExpanceButton;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
