//
//  SDHomeBaseDataModel.h
//  NestHouse
//
//  Created by shansander on 2017/3/29.
//  Copyright © 2017年 sander. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface SDHomeBaseDataModel : NSObject

@property (nonatomic, assign) CGFloat hegiht_size_cell;

@property (nonatomic, assign) BOOL show_bottom_line;


- (NSString *)CellIdentifier;

@end
