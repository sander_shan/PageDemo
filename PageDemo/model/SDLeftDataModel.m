//
//  SDLeftDataModel.m
//  PageDemo
//
//  Created by shansander on 2018/2/5.
//  Copyright © 2018年 shansander. All rights reserved.
//

#import "SDLeftDataModel.h"
#import "SDLeftTableViewCell.h"

@implementation SDLeftDataModel

- (NSString * )CellIdentifier
{
    return [SDLeftTableViewCell getCellIdentifier];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.hegiht_size_cell = DSize(100);
    }
    return self;
}

@end
