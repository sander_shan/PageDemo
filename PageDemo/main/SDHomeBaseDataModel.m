//
//  SDHomeBaseDataModel.m
//  NestHouse
//
//  Created by shansander on 2017/3/29.
//  Copyright © 2017年 sander. All rights reserved.
//

#import "SDHomeBaseDataModel.h"
#import "SDHomeBaseTableViewCell.h"

@interface SDHomeBaseDataModel ()


@end

@implementation SDHomeBaseDataModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        // 默认的高度
        self.hegiht_size_cell = 145;
        // 默认数据
        [self configData];
    }
    return self;
}

- (void)configData
{
//    [self.didBecomeActionCommand.executionSignals subscribeNext:^(id x) {
//        DLog(@"我正在执行 = %@",x);
//    }];
}


- (NSString *)CellIdentifier
{
    return [SDHomeBaseTableViewCell getCellIdentifier];
}






@end
