//
//  SDLeftDataModel.h
//  PageDemo
//
//  Created by shansander on 2018/2/5.
//  Copyright © 2018年 shansander. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SDHomeBaseDataModel.h"
@interface SDLeftDataModel : SDHomeBaseDataModel

@property (nonatomic, strong) NSString * imageView;

@property (nonatomic, strong) NSString * contentText;

@property (nonatomic, assign) BOOL isSelected;

@end
