//
//  SDHomeBaseTableViewCell.h
//  NestHouse
//
//  Created by shansander on 2017/3/29.
//  Copyright © 2017年 sander. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SDHomeBaseDataModel.h"
@interface SDHomeBaseTableViewCell : UITableViewCell

@property (nonatomic, strong) SDHomeBaseDataModel * dataModel;

@property (nonatomic, assign) NSIndexPath* indexPath;

@property (nonatomic, assign) CGFloat height_statistics_cell;

@property (nonatomic, assign) CGFloat superViewFrameWidth;



/**
 获取Cell Identifier

 @return 返回UItableViewCell的类名
 */
+ (NSString *)getCellIdentifier;


/**
 获取cell的model
 */
- (void)loadHomeDataModel:(SDHomeBaseDataModel * )model;

@property (nonatomic, weak) UITapGestureRecognizer * simpleTapGesure;

- (void)onsimpleTapAction:(UIGestureRecognizer *)gesture;

- (void)refeshFrame;


/**
 获取Cell所属的TableView

 @return super TableView
 */
- (UITableView *)supertableView;
@end
