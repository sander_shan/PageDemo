//
//  SDBaseViewModel.h
//  NestHouse
//
//  Created by shansander on 2017/5/12.
//  Copyright © 2017年 黄建国. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

@interface SDBaseViewModel : NSObject

@property (nonatomic, weak, readonly) UIViewController * viewController;


+(SDBaseViewModel *)modelWithViewController:(UIViewController *)viewController;

- (instancetype)initWithViewController:(UIViewController *)viewController;



@end
