//
//  SDCurrentViewController.h
//  SmartParking
//
//  Created by shansander on 2017/10/20.
//  Copyright © 2017年 sander. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDCurrentViewController : NSObject

+ (UIViewController *)getCurrentViewController;

/**
 * 这个获取的可能不是当前的主界面，
 */
+ (UIViewController *)getCurrentVC;

@end
