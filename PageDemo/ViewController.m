//
//  ViewController.m
//  PageDemo
//
//  Created by shansander on 2018/2/5.
//  Copyright © 2018年 shansander. All rights reserved.
//

#import "ViewController.h"
#import "SDLeftContentTableView.h"

#import <WMPageController.h>

@interface ViewController ()<WMPageControllerDataSource,WMPageControllerDelegate>

@property (nonatomic, weak)UIImageView * theBgImageview;
@property (nonatomic, weak) SDLeftContentTableView * leftContentView;

@property (nonatomic, strong) WMPageController * pageViewController;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self sd_configView];
    
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)sd_configView
{
    [self theBgImageview];
    
    [self leftContentView];
    
    self.pageViewController = [[WMPageController alloc] init];
    self.pageViewController.delegate = self;
    self.pageViewController.dataSource = self;
    self.pageViewController.view.frame = CGRectMake(DSize(120), 0, SCREEN_WIDTH - DSize(120), SCREEH_HEIGHT);
   
    self.pageViewController.view.backgroundColor = [UIColor clearColor];
    self.pageViewController.scrollView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.pageViewController.view];
    

    [self addChildViewController:self.pageViewController];
    
}
- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController
{
    return 7;
}
/**
 *  Return a controller that you wanna to display at index. You can set properties easily if you implement this methods.
 *
 *  @param pageController The parent controller.
 *  @param index          The index of child controller.
 *
 *  @return The instance of a `UIViewController`.
 */
- (__kindof UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index
{
    
    UIViewController * contentView = [[UIViewController alloc] init];
    
    contentView.view.backgroundColor = LRRandomColor;
    
    return contentView;
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView
{
    return CGRectZero;
}
- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView
{
    return CGRectMake(0, 0, SCREEN_WIDTH - DSize(120), SCREEH_HEIGHT);

}
- (UIImageView *)theBgImageview
{
    if (!_theBgImageview) {
        UIImageView * theView = [[UIImageView alloc] init];
        [self.view addSubview:theView];
        [theView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view);
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.bottom.equalTo(self.view);
        }];
        theView.image = [UIImage imageNamed:@"bgimg"];
        _theBgImageview = theView;
    }
    return _theBgImageview;
}

- (SDLeftContentTableView *)leftContentView
{
    if (!_leftContentView) {
        SDLeftContentTableView * theView = [[SDLeftContentTableView alloc] init];
        [self.view addSubview:theView];
        
        [theView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.top.equalTo(self.view);
            make.width.mas_equalTo(DSize(120));
            make.bottom.equalTo(self.view);
        }];
        _leftContentView = theView;
    }
    return _leftContentView;
}

@end
