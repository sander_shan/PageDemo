//
//  FileUtils.h
//  NestHouse
//
//  Created by shansander on 2017/5/12.
//  Copyright © 2017年 黄建国. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileUtils : NSObject
//返回缓存根目录 "caches"
+(NSString *)getCachesDirectory;

//返回根目录路径 "document"
+ (NSString *)getDocumentPath;

//创建文件夹
+(BOOL)creatDir:(NSString*)dirPath;

//删除文件夹
+(BOOL)deleteDir:(NSString*)dirPath;

//移动文件夹
+(BOOL)moveDir:(NSString*)srcPath to:(NSString*)desPath;

//创建文件
+ (BOOL)creatFile:(NSString*)filePath withData:(NSData*)data;

//读取文件
+(NSData*)readFile:(NSString *)filePath;

//删除文件
+(BOOL)deleteFile:(NSString *)filePath;


/**
 获取document下文件路径

 @param fileName 文件名字
 @return 返回 文件全路径
 */
+ (NSString*)getFilePath:(NSString*) fileName;

//在对应文件保存数据
+ (BOOL)writeDataToFile:(NSString*)fileName data:(NSData*)data;

//从对应的文件读取数据
+ (NSData*)readDataFromFile:(NSString*)fileName;

/**
 计算一个文件夹下的所有的大小

 @param folderPath 文件夹路径
 @return 文件夹的大小
 */
+ (float ) folderSizeAtPath:(NSString*) folderPath;

/**
 计算文件大小

 @param filePath 文件路径
 @return 文件大小
 */
+ (long long) fileSizeAtPath:(NSString*) filePath;


/**
  清除文件夹

 @param path 文件夹路径
 */
+(void)clearCache:(NSString *)path;



@end
