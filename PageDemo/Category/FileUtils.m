//
//  FileUtils.m
//  NestHouse
//
//  Created by shansander on 2017/5/12.
//  Copyright © 2017年 黄建国. All rights reserved.
//

#import "FileUtils.h"

@implementation FileUtils

#pragma mark - 返回缓存根目录 "caches"
+(NSString *)getCachesDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *caches = [paths firstObject];
    return caches;
}

#pragma mark - 返回根目录路径 "document"
+ (NSString *)getDocumentPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths firstObject];
    return documentPath;
}

#pragma mark - 创建文件目录
+(BOOL)creatDir:(NSString*)dirPath
{
    if ([[NSFileManager defaultManager] fileExistsAtPath:dirPath])//判断dirPath路径文件夹是否已存在，此处dirPath为需要新建的文件夹的绝对路径
    {
        return NO;
    }
    else
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:dirPath withIntermediateDirectories:YES attributes:nil error:nil];//创建文件夹
        return YES;
    }
    
}

#pragma mark - 删除文件目录
+(BOOL)deleteDir:(NSString*)dirPath
{
    if([[NSFileManager defaultManager] fileExistsAtPath:dirPath])//如果存在临时文件的配置文件
        
    {
        NSError *error=nil;
        return [[NSFileManager defaultManager]  removeItemAtPath:dirPath error:&error];
        
    }
    
    return  NO;
}

#pragma mark - 移动文件夹
+(BOOL)moveDir:(NSString*)srcPath to:(NSString*)desPath;
{
    NSError *error=nil;
    if([[NSFileManager defaultManager] moveItemAtPath:srcPath toPath:desPath error:&error]!=YES)// prePath 为原路径、     cenPath 为目标路径
    {
        NSLog(@"移动文件失败");
        return NO;
    }
    else
    {
        NSLog(@"移动文件成功");
        return YES;
    }
}

#pragma mark - 创建文件
+ (BOOL)creatFile:(NSString*)filePath withData:(NSData*)data
{
    
    return  [[NSFileManager defaultManager] createFileAtPath:filePath contents:data attributes:nil];
}

#pragma mark - 读取文件
+(NSData*)readFile:(NSString *)filePath
{
    return [NSData dataWithContentsOfFile:filePath options:0 error:NULL];
}

#pragma mark - 删除文件
+(BOOL)deleteFile:(NSString *)filePath
{
    
    return [self deleteDir:filePath];
    
}
#pragma mark - 获取document下文件路径
+ (NSString *)getFilePath:(NSString *)fileName
{
    NSString *dirPath = [[self getDocumentPath] stringByAppendingPathComponent:fileName];
    return dirPath;
}

#pragma mark - 写入document下文件的内容
+ (BOOL)writeDataToFile:(NSString*)fileName data:(NSData*)data
{
    NSString *filePath=[self getFilePath:fileName];
    return [self creatFile:filePath withData:data];
}
#pragma mark - 读取document下文件内容
+ (NSData*)readDataFromFile:(NSString*)fileName
{
    NSString *filePath=[self getFilePath:fileName];
    return [self readFile:filePath];
}

//遍历文件夹获得文件夹大小，返回多少M
+ (float ) folderSizeAtPath:(NSString*) folderPath{
    
    NSFileManager* manager = [NSFileManager   defaultManager];
    
    if (![manager  fileExistsAtPath:folderPath]) return 0;
    
    NSEnumerator *childFilesEnumerator = [[manager subpathsAtPath:folderPath] objectEnumerator];
    
    NSString* fileName;
    
    long long folderSize = 0;
    
    while ((fileName = [childFilesEnumerator nextObject]) != nil){
        
        NSString* fileAbsolutePath = [folderPath stringByAppendingPathComponent:fileName];
        
        folderSize += [self fileSizeAtPath:fileAbsolutePath];
    }
    
    return folderSize/(1024.0*1024.0);
    
}
//单个文件的大小
+ (long long) fileSizeAtPath:(NSString*) filePath{
    
    NSFileManager* manager = [NSFileManager defaultManager];
    
    if ([manager fileExistsAtPath:filePath]){
        
        return [[manager attributesOfItemAtPath:filePath error:nil]  fileSize];
    }
    return 0;
}
#pragma mark - 清除缓存的方法
+(void)clearCache:(NSString *)path{
    NSFileManager *fileManager=[NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:path]) {
        NSArray *childerFiles=[fileManager subpathsAtPath:path];
        for (NSString *fileName in childerFiles) {
            //如有需要，加入条件，过滤掉不想删除的文件
            NSString *absolutePath=[path stringByAppendingPathComponent:fileName];
            [fileManager removeItemAtPath:absolutePath error:nil];
        }
    }
    
//    // 这个是清除SDWebImage的缓存的,没有引用这个第三方类库不用写
//    [[SDImageCache sharedImageCache] cleanDisk];
}


@end
