//
//  main.m
//  PageDemo
//
//  Created by shansander on 2018/2/5.
//  Copyright © 2018年 shansander. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
