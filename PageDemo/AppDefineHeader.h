//
//  AppDefineHeader.h
//  SDEditPicture
//
//  Created by shansander on 2017/7/11.
//  Copyright © 2017年 shansander. All rights reserved.
//

#ifndef AppDefineHeader_h
#define AppDefineHeader_h

//#import "UIFont+SDFont.h"

#define kDevice_Is_iPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)

#define SCREEN_WIDTH   [UIScreen mainScreen].bounds.size.width
#define SCREEH_HEIGHT [UIScreen mainScreen].bounds.size.height


#define LRRandomColor [UIColor colorWithRed:arc4random_uniform(256)/255.0 green:arc4random_uniform(256)/255.0 blue:arc4random_uniform(256)/255.0 alpha:1.0]

#define MAXSize(float) float / 3.f

#define DFont(float) float * 1.15 / 3.f

#define DSize(float) ((float) / 2.f * SCREEN_WIDTH / 375.f)

//#define AutoPxFont(f) [UIFont autoFontWithPX:f]
//#define AutoBoldPxFont(f) [UIFont autoBoldFontWithPX:f]   //加粗字体
//#define AutoMediumPxFont(f) [UIFont autoMediumFontWithPX:f]


//iPhoneX机型判断
#define IS_iPhoneX ([UIScreen mainScreen].bounds.size.width == 375 && [UIScreen mainScreen].bounds.size.height == 812)
//导航条高度
#define NAVI_BAR_HEIGHT (IS_iPhoneX ? 88.0 : 64.0)
//状态栏高度配置
#define STATUS_BAR_HEIGHT (IS_iPhoneX? 44 : 20)
//底部安全距离
#define SAFE_BOTTOM_DISTANCE (IS_iPhoneX? 34 : 0)

#define STATUS_BAR_OFFSET (IS_iPhoneX? 24 : 0)


#define kNavigationBarHeight 44

#define kStatusBarHeight (IS_iPhoneX? 44 : 20)

#define kMarginTopHeight (IS_iPhoneX ? 88.0 : 64.0)

#define kTabBarHeight (IS_iPhoneX ? 83 : 49)



#define kNavigation_invalid_access_token  @"invalid_access_token_navigation"


#ifdef DEBUG // 调试状态, 打开LOG功能

#define SFString [NSString stringWithFormat:@"%s", __FILE__].lastPathComponent
//打印出所在文件名，所在行，堆栈地址
#define DLog(...) printf("%s: %p (line = %d): %s\n\n", [SFString UTF8String] , &self, __LINE__, [[NSString stringWithFormat:__VA_ARGS__] UTF8String]);

#else // 发布状态, 关闭LOG功能
#define DLog(s, ...)
#endif



/**
 *  强弱引用转换，用于解决代码块（block）与强引用self之间的循环引用问题
 *  调用方式: `@weakify_self`实现弱引用转换，`@strongify_self`实现强引用转换
 *
 *  示例：
 *  @weakify_self
 *  [obj block:^{
 *  @strongify_self
 *      self.property = something;
 *  }];
 */
#ifndef	weakify_self
#if __has_feature(objc_arc)
#define weakify_self autoreleasepool{} __weak __typeof__(self) weakSelf = self;
#else
#define weakify_self autoreleasepool{} __block __typeof__(self) blockSelf = self;
#endif
#endif
#ifndef	strongify_self
#if __has_feature(objc_arc)
#define strongify_self try{} @finally{} __typeof__(weakSelf) self = weakSelf;
#else
#define strongify_self try{} @finally{} __typeof__(blockSelf) self = blockSelf;
#endif
#endif


#endif /* AppDefineHeader_h */
